![](preview.gif)

# djangocms-bootstrap4

Django CMS Bootstrap 4 is a plugin bundle for django CMS providing several components from the popular Bootstrap 4 framework.

This project is based upon [djangocms-bootstrap4](https://github.com/divio/djangocms-bootstrap4).

Since some of my customers don't speak English and dangocms-bootstrap4 doesn't translate Bootstrap4 components names, I find myself wanting to translate them for the conveniance of my customers.