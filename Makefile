messages:
	django-admin makemessages -l fr --ignore=static/* --ignore=env/*

translate:
	django-admin compilemessages -l fr

migrations:
	django-admin makemigrations --no-input

migrate:
	django-admin migrate --no-input
